-- phpMyAdmin SQL Dump
-- version 4.4.12
-- http://www.phpmyadmin.net
--
-- Host: 127.0.0.1
-- Generation Time: Feb 10, 2017 at 01:56 PM
-- Server version: 5.6.25
-- PHP Version: 5.6.11

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `clearance`
--

-- --------------------------------------------------------

--
-- Table structure for table `academic`
--

CREATE TABLE IF NOT EXISTS `academic` (
  `id` int(11) NOT NULL,
  `studentid` varchar(10) NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00' ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `academic`
--

INSERT INTO `academic` (`id`, `studentid`, `created_at`, `updated_at`) VALUES
(2, '02410113', '2017-02-10 12:48:32', '0000-00-00 00:00:00');

-- --------------------------------------------------------

--
-- Table structure for table `department`
--

CREATE TABLE IF NOT EXISTS `department` (
  `id` int(11) NOT NULL,
  `studentid` varchar(10) NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00' ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `department`
--

INSERT INTO `department` (`id`, `studentid`, `created_at`, `updated_at`) VALUES
(1, '02110113', '2017-02-07 12:32:00', '0000-00-00 00:00:00'),
(2, '02410113', '2017-02-10 12:51:35', '0000-00-00 00:00:00');

-- --------------------------------------------------------

--
-- Table structure for table `ess`
--

CREATE TABLE IF NOT EXISTS `ess` (
  `id` int(11) NOT NULL,
  `studentid` varchar(10) NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00' ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `examunit`
--

CREATE TABLE IF NOT EXISTS `examunit` (
  `id` int(11) NOT NULL,
  `studentid` varchar(10) NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00' ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `finance`
--

CREATE TABLE IF NOT EXISTS `finance` (
  `id` int(11) NOT NULL,
  `studentid` varchar(10) NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00' ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `finance`
--

INSERT INTO `finance` (`id`, `studentid`, `created_at`, `updated_at`) VALUES
(1, '02110113', '2017-02-07 04:40:59', '0000-00-00 00:00:00');

-- --------------------------------------------------------

--
-- Table structure for table `library`
--

CREATE TABLE IF NOT EXISTS `library` (
  `id` int(11) NOT NULL,
  `studentid` varchar(10) NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00' ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `registry`
--

CREATE TABLE IF NOT EXISTS `registry` (
  `id` int(11) NOT NULL,
  `studentid` varchar(10) NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00' ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `studentaffairs`
--

CREATE TABLE IF NOT EXISTS `studentaffairs` (
  `id` int(11) NOT NULL,
  `studentid` varchar(10) NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00' ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `students`
--

CREATE TABLE IF NOT EXISTS `students` (
  `sid` int(11) NOT NULL,
  `studentid` varchar(50) DEFAULT NULL,
  `surname` varchar(255) DEFAULT NULL,
  `othernames` varchar(255) NOT NULL,
  `society` varchar(255) DEFAULT NULL,
  `email` varchar(255) DEFAULT NULL,
  `gender` enum('Male','Female','','') NOT NULL,
  `nationality` varchar(255) NOT NULL,
  `level` varchar(3) NOT NULL,
  `session` varchar(255) NOT NULL,
  `prog` varchar(255) NOT NULL,
  `password` varchar(255) DEFAULT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00'
) ENGINE=InnoDB AUTO_INCREMENT=23 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `students`
--

INSERT INTO `students` (`sid`, `studentid`, `surname`, `othernames`, `society`, `email`, `gender`, `nationality`, `level`, `session`, `prog`, `password`, `created_at`, `updated_at`) VALUES
(1, '02110113', 'Okeke', 'Toby', 'Stott', 'toby.okeke@regent.edu.gh', 'Male', '', '400', '', 'ISS', 'Tobyokeke1', '2017-02-07 11:37:58', '0000-00-00 00:00:00'),
(2, '02260113', 'Asare', 'Ben', 'ubuntu', 'benjamin.asare@gmail.com', 'Male', '', '400', '', 'ISS', '1234567', '2017-02-07 11:38:01', '0000-00-00 00:00:00'),
(13, '02232223', 'St Patrick', 'James', 'Stott', 'james@stpatrick.com', 'Male', 'American', '400', 'MORNING', 'ECONOMICS', 'SCYFIA', '2017-02-07 11:38:04', '2017-01-14 11:21:07'),
(16, '02201102', 'Oluwola', 'James', 'Ubuntu', 'oluwola@regent.com', 'Male', 'Nigerian', '400', 'MORNING', 'COMPUTER SCIENCE', '7L2hIU', '2017-02-07 11:38:09', '2017-01-14 12:04:33'),
(17, '02202233', 'Ben', 'Asante', 'Stott', 'ben@asante.com', 'Female', 'Ghanaian', '400', 'EVENING', 'ENGINEERING', 'kZo6xa', '2017-02-07 11:38:15', '2017-01-14 12:04:33'),
(20, '02110113', 'Okeke', 'James', 'Stot', 'jamesokeke@regent.edu.gh', 'Male', 'Nigerian', '400', 'Evening', 'ISS', 'k6yDir', '2017-02-10 07:15:08', '2017-02-10 07:15:08'),
(21, '02410113', 'St Patrick', 'Shiro', 'Stott', 'shiro@regent.edu.gh', 'Male', 'Nigerian', '400', 'Evening', 'ISS', 'YIkxRH', '2017-02-10 11:02:04', '2017-02-10 11:02:04'),
(22, '02410113', 'Tanga', 'Samuel', 'Stott', 'tanga@regent.edu.gh', 'Male', 'Ghanaian', '400', 'Morning', 'Computer Science', 'CDlZ4G', '2017-02-10 11:39:51', '2017-02-10 11:39:51');

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE IF NOT EXISTS `users` (
  `lid` int(11) NOT NULL,
  `staffid` varchar(50) DEFAULT NULL,
  `name` varchar(255) DEFAULT NULL,
  `qualification` varchar(255) DEFAULT NULL,
  `phone` varchar(255) DEFAULT NULL,
  `email` varchar(255) DEFAULT NULL,
  `role` enum('EXAM UNIT','FINANCE','DEPARTMENT','ACADEMIC','STUDENT AFFAIRS','LIBRARY','ESS','REGISTRY') DEFAULT NULL,
  `password` varchar(255) DEFAULT NULL,
  `remember_token` varchar(255) DEFAULT NULL,
  `did` int(11) NOT NULL,
  `lastLogin` timestamp NULL DEFAULT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00'
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`lid`, `staffid`, `name`, `qualification`, `phone`, `email`, `role`, `password`, `remember_token`, `did`, `lastLogin`, `created_at`, `updated_at`) VALUES
(4, '02110113', 'Toby Okeke', 'Phd', '02911221323', 'toby.okeke@gmail.com', 'EXAM UNIT', '$2y$10$Nn8namzmLXuQPhp0yU3XLOZ412RVDtyGnvDSpne3RiTJMr6KuxmJK', 'R8MOsCgZWprMAsePTAc9udP9ku1JFN4jEOhJeKMNOAzfln2Gvkz98aVpVrFf', 1, '2017-02-06 13:00:06', '2017-02-10 12:39:58', '2017-02-06 13:00:06'),
(5, '02260113', 'benjamin asare  danquah', 'phd', '0269527587', 'ben@gmail.com', 'LIBRARY', '$2y$10$6kC3LzsonuSF/NYYJSNZb.b5xaxJCyZqgxWSU9Qi/su2NgAfIdTbm', NULL, 3, NULL, '2017-02-07 12:21:34', '2017-02-06 10:52:08');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `academic`
--
ALTER TABLE `academic`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `department`
--
ALTER TABLE `department`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `ess`
--
ALTER TABLE `ess`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `examunit`
--
ALTER TABLE `examunit`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `finance`
--
ALTER TABLE `finance`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `library`
--
ALTER TABLE `library`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `registry`
--
ALTER TABLE `registry`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `studentaffairs`
--
ALTER TABLE `studentaffairs`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `students`
--
ALTER TABLE `students`
  ADD PRIMARY KEY (`sid`),
  ADD KEY `progid` (`prog`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`lid`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `academic`
--
ALTER TABLE `academic`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT for table `department`
--
ALTER TABLE `department`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT for table `ess`
--
ALTER TABLE `ess`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `examunit`
--
ALTER TABLE `examunit`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `finance`
--
ALTER TABLE `finance`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `library`
--
ALTER TABLE `library`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `registry`
--
ALTER TABLE `registry`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `studentaffairs`
--
ALTER TABLE `studentaffairs`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `students`
--
ALTER TABLE `students`
  MODIFY `sid` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=23;
--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
  MODIFY `lid` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=6;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;

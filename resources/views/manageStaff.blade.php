@extends('layouts.app')

@section('content')

    <div class=" row">


        @include('sidebar')
        <div align="center" class="col-md-10 main col-md-offset-2  ">


            @if( Session::has('success') )
                <div class="alert alert-success" align="center">{{Session::get('success')}}</div>
            @endif


            @if( Session::has('error') )
                <div class="alert alert-danger" align="center">{{Session::get('error')}}</div>
            @endif


            <div class="logoStuff">
                <img src="{{url('/images/logo.png')}}" class="logo">
                <h3 class="logoHeader">Regent Online Clearance System <br> All Staff</h3>
            </div>


            <table class="table table-responsive">
                <tr>
                    <th>Name</th>
                    <th>Phone</th>
                    <th>Email</th>
                    <th>Role</th>
                    <th>Society</th>
                    <th></th>
                </tr>

                @foreach($staff as $item)
                    <tr>
                        <td>{{$item->name}}</td>
                        <td>{{$item->phone}}</td>
                        <td>{{$item->email}}</td>
                        <td>{{$item->role}}</td>
                        <td>
                            @if(isset($item->societyName))
                            {{$item->societyName}}
                                @else
                                Does not apply
                            @endif
                        </td>

                        <td>

                            <a href="{{url('/admin/delete/'. $item->lid)}}" class="btn btn-danger">Delete</a>
                        </td>


                    </tr>
                @endforeach


            </table>

        </div>
    </div>

@endsection
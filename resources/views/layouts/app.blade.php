<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>ONLINE CLEARANCE PORTAL | REGENT UNIVERSITY COLLEGE OF SCIENCE AND TECHNOLOGY</title>

    <!-- Styles -->
    <link href="{{url('css/app.css')}}" rel="stylesheet">
    <link href="{{url('css/style.css')}}" rel="stylesheet">
    <link rel="stylesheet" href="{{url('css/font-awesome.min.css')}}">


    <!-- Scripts -->
    <script src="{{url('js/jquery.min.js')}}"></script>


    <script>
        window.load = function(){
            document.getElementById("sidebar").style.height = window.innerHeight;
        }
        window.Laravel = {!! json_encode([
            'csrfToken' => csrf_token(),
        ]) !!};
    </script>
</head>
<body>
    <div id="app">
        <nav class="navbar navbar-default navbar-static-top">
            <div class="container">
                <div class="navbar-header">

                    <!-- Collapsed Hamburger -->
                    <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#app-navbar-collapse">
                        <span class="sr-only">Toggle Navigation</span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                    </button>

                    <!-- Branding Image -->
                    <a class="navbar-brand" href="{{ url('/') }}">
                        ONLINE CLEARANCE PORTAL
                    </a>
                </div>

                <div class="collapse navbar-collapse" id="app-navbar-collapse">
                    <!-- Left Side Of Navbar -->
                    <ul class="nav navbar-nav">
                        &nbsp;


                        @if(!Auth::guest())
                            <li class="hidden-md hidden-lg ">
                                @if(Auth::user()->role == "EXAM UNIT")
                                    <a href="{{url('/staff/upload-students')}}">UPLOAD STUDENTS</a>
                                @else
                                    @if(Auth::user()->role != "ADMIN")
                                        <a href="{{url('/staff/clear-students')}}">CLEAR STUDENTS</a>
                                    @else
                                        <a href="{{url('/register')}}">REGISTER STAFF</a>
                                    @endif
                                @endif
                            </li>
                            @if(Auth::user()->role != "EXAM UNIT")
                                <li class="hidden-md hidden-lg ">
                                    <a href="{{url('/')}}">
                                        VIEW UNCLEARED STUDENTS
                                    </a>
                                </li>

                                <li class="hidden-md hidden-lg ">
                                    <a href="{{url('/staff/view-cleared')}}">
                                        VIEW CLEARED STUDENTS
                                    </a>
                                </li>

                            @endif

                        <li>
                            <a href="{{url('/staff/view-reports')}}">VIEW REPORTS</a>
                        </li>

                        @endif

                    </ul>

                    <!-- Right Side Of Navbar -->
                    <ul class="nav navbar-nav navbar-right">
                        <!-- Authentication Links -->

                        @if(!\Illuminate\Support\Facades\Session::has('student'))
                            @if (Auth::guest())
                                <li><a href="{{ route('login') }}">Staff Login</a></li>
                                <li><a href="{{ url('/student/login') }}">Student Login</a></li>
                            @else
                                <li class="dropdown">
                                    <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false">

                                       {{Auth::user()->role}} : {{ Auth::user()->staffid }} <span class="caret"></span>
                                    </a>

                                    <ul class="dropdown-menu" role="menu">
                                        <li>
                                            <a href="{{ route('logout') }}"
                                                onclick="event.preventDefault();
                                                         document.getElementById('logout-form').submit();">
                                                Logout
                                            </a>

                                            <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                                                {{ csrf_field() }}
                                            </form>
                                        </li>
                                    </ul>
                                </li>
                            @endif

                        @else
                            <li class="dropdown">
                                <a href="#"  class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false">
                                    ID Num: <span id="studentid"> {{ Session::get('student')->studentid }} </span> <span class="caret"></span>
                                </a>

                                <ul class="dropdown-menu" role="menu">
                                    <li>
                                        <a href="{{ route('logout') }}"
                                           onclick="event.preventDefault();
                                                         document.getElementById('logout-form').submit();">
                                            Logout
                                        </a>

                                        <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                                            {{ csrf_field() }}
                                        </form>
                                    </li>
                                </ul>
                            </li>
                        @endif
                    </ul>
                </div>
            </div>
        </nav>

        @yield('content')

    </div>

    <!-- Scripts -->
    <script src="{{url('js/app.js')}}"></script>
</body>
</html>

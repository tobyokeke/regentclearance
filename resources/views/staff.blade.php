@extends('layouts.app')

@section('content')

    <?php use Illuminate\Support\Facades\Input; use Illuminate\Support\Facades\Session; ?>


    <div class=" row">

        @include('sidebar')

        <div align="center" class="col-md-10 main col-md-offset-2 " >
            <div class="logoStuff">
                <img src="{{url('/images/logo.png')}}" class="logo">
                <h3 class="logoHeader">Regent Online Clearance System</h3>
            </div>

            @if( Session::has('success') )
                <div class="alert alert-success" align="center">{{Session::get('success')}}</div>
            @endif


            @if( Session::has('error') )
                <div class="alert alert-danger" align="center">{{Session::get('error')}}</div>
            @endif

            <h3 style="color:#B79043">UNCLEARED STUDENTS</h3>
            <table class="table table-hover">
                <tr>
                    <th>Student ID</th>
                    <th>Surname</th>
                    <th>Other Names</th>
                    <th>Gender</th>
                    <th>Nationality</th>
                    <th>Programme</th>

                </tr>
                @foreach($allStudents as $item)
                    @foreach($students as $clearedID)
                        @if($item->studentid != $clearedID->studentid && Auth::user()->role != "EXAM UNIT")

                    <tr>
                        <td>{{$item->studentid}}</td>
                        <td>{{$item->surname}}</td>
                        <td>{{$item->othernames}}</td>
                        <td>{{$item->gender}}</td>
                        <td>{{$item->nationality}}</td>
                        <td>{{$item->prog}}</td>

                    </tr>
                        @endif
                    @endforeach

                @endforeach

            </table>

                <h3>There are {{count(\App\student::all())}} prospective graduands</h3>

            @if(Auth::user()->role != "EXAM UNIT")
                <h3>{{count(\App\student::all()) - $total}} are uncleared</h3>
            @endif

                <nav aria-label="Page navigation">
                    <ul class="pagination">

                        @if($page > 1)
                        <li>
                            <a href="{{url('/?page=' . ($page -1) )}}" aria-label="Previous">
                                Previous Page
                                <span aria-hidden="true">&laquo;</span>
                            </a>
                        </li>


                        @endif

                        @if($page >= $max)

                        @else
                        <li>
                            <a href="{{url('/?page=' . ($page + 1)  )}}" aria-label="Next">
                                Next Page
                                <span aria-hidden="true">&raquo;</span>
                            </a>
                        </li>
                        @endif
                    </ul>
                    </nav>
        </div>



    </div>

@endsection
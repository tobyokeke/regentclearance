@extends('layouts.app')

@section('content')

    <?php use Illuminate\Support\Facades\Input; use Illuminate\Support\Facades\Session; ?>


    <div class=" row">

        @include('sidebar')

        <div align="center" class="col-md-10 main col-md-offset-2 " >
            <div class="logoStuff">
                <img src="{{url('/images/logo.png')}}" class="logo">
                <h3 class="logoHeader">Regent Online Clearance System</h3>
            </div>

            @if( Session::has('success') )
                <div class="alert alert-success" align="center">{{Session::get('success')}}</div>
            @endif


            @if( Session::has('error') )
                <div class="alert alert-danger" align="center">{{Session::get('error')}}</div>
            @endif

            <h3 style="display:inline; color:#B79043">FULLY CLEARED STUDENTS : </h3>
            <a href="{{url('view-cleared-report')}}" class="btn btn-primary">Download PDF</a> <br> <br>

            <h3 style="display:inline; color:#B79043">NOT CLEARED STUDENTS : </h3>
            <a href="{{url('view-uncleared-report')}}" class="btn btn-primary">Download PDF</a> <br> <br>


            <h3>There are {{count(\App\student::all())}} fully cleared graduands</h3>



        </div>



    </div>

@endsection
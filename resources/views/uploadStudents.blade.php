@extends('layouts.app')

@section('content')

    <div class=" row">


        @include('sidebar')
        <div align="center" class="col-md-10 main col-md-offset-2 ">


            @if( Session::has('success') )
                <div class="alert alert-success" align="center">{{Session::get('success')}}</div>
            @endif


            @if( Session::has('error') )
                <div class="alert alert-danger" align="center">{{Session::get('error')}}</div>
            @endif


                <div class="logoStuff">
                <img src="{{url('/images/logo.png')}}" class="logo">
                <h3 class="logoHeader">Regent Online Clearance System</h3>
            </div>

            <h3>There are {{$total}} uploaded students</h3>


            <div>
                <!-- Nav tabs -->
                <ul class="nav nav-tabs col-md-4 col-md-offset-4" role="tablist">
                    <li role="presentation" class="active"><a href="#single" aria-controls="home" role="tab" data-toggle="tab">UPLOAD</a></li>

                </ul>

                @if(Auth::user()->role == "EXAM UNIT")
                        <!-- Tab panes -->
                <div class="color2 tab-content col-md-4 col-md-offset-4">
                    <div role="tabpanel" class="tab-pane active" id="single">
                        <form class="form" method="post" enctype="multipart/form-data" action="{{url('/bulk-add-students')}}">
                            {{csrf_field()}}

                            <label>Select file with student details:</label>
                            <input type="file" name="file">

                            <button type="submit" class="btn btn-primary">ADD</button>
                        </form>
                    </div>
                </div>

                @else

                        <!-- Tab panes -->
                <div class="tab-content">
                    <div role="tabpanel" class="tab-pane active" id="single">
                        <form class="form" method="post" action="{{url('/clear-student')}}">
                            {{csrf_field()}}
                            <div class="form-group">
                                <label for="studentid">Student ID Number:</label>
                                <input type="text" class="form-control" id="studentid" name="studentid">
                            </div>

                            <button type="submit" class="btn btn-default">ADD</button>
                        </form>
                    </div>
                    <div role="tabpanel" class="tab-pane" id="bulk">
                        <form class="form-inline" method="post" enctype="multipart/form-data" action="{{url('/bulk-clear-students')}}">
                            {{csrf_field()}}
                            <div class="form-group">
                                <label for="file" style="float:left">File:</label>
                                <input type="file" id="file" name="file" style="float:left">
                            </div>
                            <button type="submit" class="btn btn-default">UPLOAD</button>
                        </form>

                    </div>
                </div>

                @endif

            </div>

        </div>
    </div>
@endsection
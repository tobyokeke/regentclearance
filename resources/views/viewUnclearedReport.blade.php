<html>
<head>

</head>
<body>
<div align="center" class="col-md-10 main col-md-offset-2 " >
	<div class="logoStuff">
		<img src="{{url('/images/logo.png')}}" style="height: 100px;" class="logo">
		<h3 class="logoHeader">Regent Online Clearance System</h3>
	</div>

	<style>
		td{
			text-align: center;
		}

	</style>

	@if( Session::has('success') )
	<div class="alert alert-success" align="center">{{Session::get('success')}}</div>
	@endif


	@if( Session::has('error') )
	<div class="alert alert-danger" align="center">{{Session::get('error')}}</div>
	@endif

	<h3 style="color:#B79043">UNCLEARED STUDENTS</h3>
	<table  class="table ">
		<tr >
			<th>Student ID</th>
			<th>Surname</th>
			<th>Other Names</th>
			<th>Gender</th>
			<th>Nationality</th>
			<th>Programme</th>

		</tr>
		@foreach($students as $item)

		<tr>
			<td>{{$item->studentid}}</td>
			<td>{{$item->surname}}</td>
			<td>{{$item->othernames}}</td>
			<td>{{$item->gender}}</td>
			<td>{{$item->nationality}}</td>
			<td>{{$item->prog}}</td>

		</tr>

		@endforeach

	</table>

</div>
</body>
</html>
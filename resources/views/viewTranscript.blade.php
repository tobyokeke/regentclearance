@extends('layouts.app')

@section('content')



    <div class=" row">

        @include('sidebar')

        <div align="center" class="col-md-10 main col-md-offset-2 " >
            <div class="logoStuff">
                <img src="{{url('/images/logo.png')}}" class="logo">
                <h3 class="logoHeader">Regent Online Clearance System</h3>
            </div>

            @if( Session::has('success') )
                <div class="alert alert-success" align="center">{{Session::get('success')}}</div>
            @endif


            @if( Session::has('error') )
                <div class="alert alert-danger" align="center">{{Session::get('error')}}</div>
            @endif

            <h3 style="color:#B79043">VIEW TRANSCRIPT</h3>
            <table class="table table-hover">
                <tr>
                    <th>Student ID</th>
                    <th>Surname</th>
                    <th>Other Names</th>
                    <th></th>

                </tr>
                @foreach($allStudents as $item)


                        @if($item->transcript != "")
                            <tr>
                                <td>{{$item->studentid}}</td>
                                <td>{{$item->surname}}</td>
                                <td>{{$item->othernames}}</td>
                                <td>

                                    <a href="{{$item->transcript}}" class="btn btn-primary">DOWNLOAD</a>
                                </td>

                            </tr>


                        @endif


                @endforeach

            </table>

            <h3>There are {{count(\App\student::all())}} prospective graduands</h3>

            @if(Auth::user()->role != "EXAM UNIT")
                <h3>{{$total}} have been cleared by this department</h3>
            @endif

            <nav aria-label="Page navigation">
                <ul class="pagination">

                    @if($page > 1)
                        <li>
                            <a href="{{url('/staff/view-transcript?page=' . ($page -1) )}}" aria-label="Previous">
                                Previous Page
                                <span aria-hidden="true">&laquo;</span>
                            </a>
                        </li>


                    @endif

                    @if($page >= $max)

                    @else
                        <li>
                            <a href="{{url('/staff/view-transcript?page=' . ($page + 1)  )}}" aria-label="Next">
                                Next Page
                                <span aria-hidden="true">&raquo;</span>
                            </a>
                        </li>
                    @endif
                </ul>
            </nav>
        </div>



    </div>


@endsection
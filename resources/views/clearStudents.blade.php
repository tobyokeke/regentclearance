@extends('layouts.app')

@section('content')

    <div class=" row">

        @include('sidebar')
        <div align="center" class="col-md-10 main col-md-offset-2" style="margin-top: 100px">
            <div class="logoStuff">
                <img src="{{url('/images/logo.png')}}" class="logo">
                <h3 class="logoHeader">Regent Online Clearance System</h3>
            </div>


            @if( Session::has('success') )
                <div class="alert alert-success" align="center">{{Session::get('success')}}</div>
            @endif


            @if( Session::has('error') )
                <div class="alert alert-danger" align="center">{{Session::get('error')}}</div>
            @endif



            <h3 style="color:#B79043">PROSPECTIVE GRADUANDS</h3>
            <table class="table table-hover">
                <tr>
                    <th>Student ID</th>
                    <th>Surname</th>
                    <th>Other Names</th>
                    <th>Gender</th>
                    <th>Nationality</th>
                    <th>Programme</th>
                    <th></th>

                </tr>
                @foreach($allStudents as $item)
                    <tr>
                        <td>{{$item->studentid}}</td>
                        <td>{{$item->surname}}</td>
                        <td>{{$item->othernames}}</td>
                        <td>{{$item->gender}}</td>
                        <td>{{$item->nationality}}</td>
                        <td>{{$item->prog}}</td>
                        <td>
                            @foreach($students as $clearedID)
                                @if($item->studentid == $clearedID->studentid && Auth::user()->role != "EXAM UNIT")
                                    <span class="btn btn-success">CLEARED</span>
                                @endif
                            @endforeach

                            @foreach($allStudents->diff($students) as $clearedID)
                                @if($item->studentid == $clearedID->studentid && Auth::user()->role != "EXAM UNIT")
                                        <form class="form form-inline" style="padding: 0;" method="post" action="{{url('/clear-student')}}">
                                            {{csrf_field()}}
                                                <input type="hidden" class="form-control" value="{{$item->studentid}}" name="studentid">
                                            <button type="submit" style="background-color: #2579A9" class="btn btn-primary">CLEAR STUDENT</button>
                                        </form>
                                @endif
                            @endforeach


                        </td>
                    </tr>
                @endforeach

            </table>

            <h3>There are {{count(\App\student::all()->where('society',Auth::user()->societyName))}} prospective graduands</h3>

            @if(Auth::user()->role != "EXAM UNIT")
                <h3>{{$total}} have been cleared by this department</h3>
            @endif

            <nav aria-label="Page navigation">
                <ul class="pagination">

                    @if($page > 1)
                        <li>
                            <a href="{{url('/staff/clear-students?page=' . ($page -1) )}}" aria-label="Previous">
                                Previous Page
                                <span aria-hidden="true">&laquo;</span>
                            </a>
                        </li>


                    @endif

                    @if($page >= $max)

                    @else
                        <li>
                            <a href="{{url('/staff/clear-students?page=' . ($page + 1)  )}}" aria-label="Next">
                                Next Page
                                <span aria-hidden="true">&raquo;</span>
                            </a>
                        </li>
                    @endif
                </ul>
            </nav>

         @if(Auth::user()->role == "EXAM UNIT")
                 <!-- Tab panes -->
         <div class="color2 tab-content col-md-4 col-md-offset-4">
             <div role="tabpanel" class="tab-pane active" id="single">
                 <form class="form" method="post" enctype="multipart/form-data" action="{{url('/bulk-add-students')}}">
                     {{csrf_field()}}

                     <label>Select file with student details:</label>
                     <input type="file" name="file">

                     <button type="submit" class="btn btn-primary" style="background-color: #2579A9">CLEAR</button>
                 </form>
             </div>
         </div>

         @else

                 <!-- Tab panes -->
         <div class="tab-content col-md-4 col-md-offset-4">
             <div role="tabpanel" class="tab-pane active" id="single">
             </div>
             <div role="tabpanel" class="tab-pane" id="bulk">
                 {{--<form class="form-inline" method="post" enctype="multipart/form-data" action="{{url('/bulk-clear-students')}}">--}}
                     {{--{{csrf_field()}}--}}
                     {{--<div class="form-group">--}}
                         {{--<label for="file" style="float:left">File:</label>--}}
                         {{--<input type="file" id="file" name="file" style="float:left">--}}
                     {{--</div>--}}
                     {{--<button type="submit" class="btn btn-default">UPLOAD</button>--}}
                 {{--</form>--}}

             </div>
         </div>

         @endif

     </div>

    </div>

@endsection
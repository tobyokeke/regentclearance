<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Support\Facades\Auth;

class admin
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
	    if(!Auth::guest()) {
		    if ( Auth::user()->role == "ADMIN" ) {
			    return $next( $request );
		    }
	    }
	    return redirect('/login');
    }
}

<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/


Auth::routes();

Route::get('/','LecturerController@index');
Route::get('/student/login','HomeController@getLogin');


Route::get('/staff','LecturerController@index');
Route::get('/staff/clear-students','LecturerController@getClearStudents');
Route::get('/staff/upload-students','LecturerController@getUploadStudents');
Route::get('/staff/unclear/{sid}','LecturerController@unclearStudent');
Route::get('/staff/view-transcript','LecturerController@viewTranscript');
Route::get('/staff/view-cleared','LecturerController@viewCleared');
Route::get('/staff/view-reports','LecturerController@viewReports');
Route::get('/view-cleared-report','LecturerController@downloadClearedPdf');
Route::get('/view-uncleared-report','LecturerController@downloadUnclearedPdf');
Route::get('/home', 'StudentController@index');
Route::get('/cleared/{id}','StudentController@getCleared');




Route::post('/initiate','StudentController@initiate');
Route::post('/clear-student','LecturerController@postClearStudent');
Route::post('/bulk-clear-students','LecturerController@postBulkClearStudent');
Route::post('/student/login','HomeController@postLogin');
Route::post('/add-students','LecturerController@postAddStudents');
Route::post('/bulk-add-students','LecturerController@postBulkAddStudents');


// admin routes
Route::get('/admin','LecturerController@getStaff');
Route::get('/admin/delete/{lid}','LecturerController@deleteUser');